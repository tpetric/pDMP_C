
#ifndef _PERIODIC_DMP_H_

#define _PERIODIC_DMP_H_

#include <string>
// #include <time.h>
#include <fstream>
#include <sstream>
#include <iostream>

#include "utility.h"

#define STRING_MAX 256

class pDMP_structure {
private:
    // Basic setup
    int flag;
    int dof;
    
    // pDMP - periodicDMP parameters
    double alpha_z, beta_z;     // DMP gains
    int N;                   // number of basis functions
    double lambda;              // forgetting factor
    double **w;                 // Weights
    double **P;
    double *c;                  // basis functions
    double *f;                  // nonlinear turn
    double *r;                  // amplitude parameter
    
    // Current state for integration
    double *y;
    double *z;
  
    // Goal (anchor of the oscilation)
    double *g;
    
    // Phase and frequency scaling
    double *phi;
    double *tau;                // tau = 1/frequency
    

public:
    pDMP_structure(char *file_name);
    pDMP_structure(int dof, int N, double alpha_z, double beta_z, double lambda);
    ~pDMP_structure();

    void pDMP_param_print(void);
    
    
    void pDMP_calculate_f(double *y_in_, double *dy_in_, double *ddy_in_);
    void pDMP_calculate_f(void);
    void pDMP_calculate_f(double *Ct_); // Coatching update !!!
    
    
    void define_basis_functions(int N, double factor);
    
    void pDMP_integrate(double dt);
    
    double get_position(int i);
    void set_int_position(double y_init, int dof);
    double get_velocity(int i);
    double get_acceleration(int i);
    
    void get_weights(int j, double *output);
    void set_weights(int j, double *input);
    
    double get_phase(int i);
    double get_tau(int i);

    void set_phase(int i, double phi_);
    void set_tau(int i, double tau_);
    
    int get_dof() { return dof; };
    int get_num_basis() { return N;};
    
    int get_flag() { return flag;};
    
    void set_flag(int flag_) {flag = flag_;};
    
    void save_DMP(char *file_name);
    void load_DMP(char *file_name);
    
    // Coatching stuff
    double norm(double a[3]);
    double sig(double x);
    double sig(double x, double d_min, double d_max,double d_off, double ni);
    void vector_norm(double a[3], double a_n[3]);
    double vector_angle(double a[3], double b[3]);
    void Ct(double y[3], double dy[3], double o[3], double dir[3], double Ct_[3]);
};

#endif
