/*============================================================================
 ==============================================================================
 
 coaching_pDMP.cpp
 
 Date:  August 2013
 Authors: Tadej Petric
 ==============================================================================
 Remarks:
 
 Coaching - generate new periodic DMP
 
 ============================================================================*/

// SL system headers
#include "SL_system_headers.h"

// SL includes
#include "SL.h"
#include "SL_user.h"
#include "SL_tasks.h"
#include "SL_task_servo.h"
#include "SL_kinematics.h"
#include "SL_dynamics.h"
#include "SL_collect_data.h"
#include "SL_shared_memory.h"
#include "SL_man.h"
#include "SL_filters.h"
#include "SL_common.h"
#include "SL_objects.h"
#include "SL_userGraphics.h"

#include <fcntl.h>

// local includes
#include "coaching_pDMP.h"

static  struct {
    float    pos[N_CART+1];
    float    scale[N_CART+1];
} cube;

#define MAX_BUFFER  10000
#define KINECT_FREQ 30
static const char *fifo_name = "KinectReaderFIFO";
static int Kinect_pipe = 0;

// data files
static char DMP_file_name[] = PREFS "pDMP_Task.txt";
static char DMP_file_name_new[] = PREFS "pDMP_Task_New.txt";

static char Kinect_file_name[] = PREFS "Kinect_cal.txt";

char buffer[MAX_BUFFER];

static int servo_rate = SERVO_BASE_RATE / TASK_SERVO_RATIO;

static double o[] = {0.5, 0.4, 0.2};
static double o_R[] = {0.5, 0.3, 0.2};
static double o_L[] = {0.5, 0.4, 0.2};
static double dir[] = {-1, 0, 0};
static double dir_set[] = {-1, 0, 0};
static double epsilon = 1.0e-8;

#define SQR(a) ((a)*(a))

coaching_pDMP_Task::coaching_pDMP_Task() : start_time_(0)
{
    if (Kinect_pipe != 0)
    {
        close(Kinect_pipe);
        Kinect_pipe = 0;
    }
    // clear global variables
    active_dofs = NULL;
    ndofs = 0;

    pDMP_object = NULL;
    AFS_object = NULL;
}

coaching_pDMP_Task::~coaching_pDMP_Task()
{
    if (Kinect_pipe > 0)
        close(Kinect_pipe);
    if (pDMP_object != NULL)
        delete pDMP_object;
    if (active_dofs != NULL)
        delete [] active_dofs;
    if (AFS_object != NULL)
        delete AFS_object;
}

int coaching_pDMP_Task::initialize()
{
    double tmp;
    int i;
    int ans = 999;

    start_time_ = 0.0;
    step = 0;
  
    freezeBase(1);
    
    ndofs = 6;
    
    // prepare Kinect communication
    if (!KinectReader(fifo_name, NULL))
    {
        printf("Communication with Kinect could not be established!\n");
        //return FALSE;
    }

    //printf("%d %d %d %d %d\n", active_dofs, AFS_object, pDMP_object, NULL, step);
    
    // Define which DOFs are active
    if (active_dofs != NULL)
      delete [] active_dofs;
    active_dofs = new int[ndofs+1];
    active_dofs[0] = ndofs;
    active_dofs[1] = R_SFE;
    active_dofs[2] = R_SAA;
    active_dofs[3] = R_HR;
    active_dofs[4] = R_EB;
    active_dofs[5] = R_WR;
    active_dofs[6] = R_WFE;
    // active_dofs[7] = R_WAA;
    
    // Define dt
    dt = 1.0 / servo_rate;

    // Prepare oscillator structure and inital conditions
    // We need this for generating the phase for pDMPs
    if (AFS_object != NULL)
        delete AFS_object;
    // 1 - dof, 10 M - size of the Fourier series, 2 - ni gain, 20 K - gain
    AFS_object = new AFS_structure(1, 10, 2, 20);
    // Set inital freqeuncy
    AFS_object->set_initial_AFS_state(PI);
    // Set to oscilate - 0 (1 is for adapting)
    AFS_object->set_flag(0);
    // AFS_object->AFS_param_print();

    // Hack for now, otherwise crashes
    pDMP_object = NULL;

    // Prepare periodic DMP structure and initial conditions
    if (pDMP_object != NULL)
        delete pDMP_object;
    // dof, N, alpha, beta, lambda

    pDMP_object = new pDMP_structure(ndofs, 25, 8, 2, 0.9995);
    // Repeat 0 (1 Learn new trajectory)
    pDMP_object->set_flag(0);
    // Load from file
    pDMP_object->load_DMP(DMP_file_name);
    //pDMP_object->pDMP_param_print();

    ans = 999;
    // Calibrate Kinect
    while (ans != 1 && ans != 0)
    {
        get_int(const_cast<char*>("Enter 1 to calibrate or 0 to load from file"),0,&ans);
    }
    
    if (ans == 1)
    {
        Calibrate_Procedure();
        save_Kinect(Kinect_file_name);
    }
    else
    {
        load_Kinect(Kinect_file_name);
    }
    distance = 0.0;

    // prepare going to the default posture
    bzero((char *)&(target_[1]),N_DOFS*sizeof(target_[1]));
    for (int i=1; i<=N_DOFS; i++)
    {
        target_[i] = joint_default_state[i];
    }
    //
    //target_[B_TFE].th = 0.2;
    //target_[B_HN].th = 0.2;
    
    // default state for RightHand based on loaded pDMPs
    for(int i=1; i<=ndofs; i++)
    {
        //target_[active_dofs[i]].th = pDMP_object->get_position(i);   // set robot position to init DMP
        pDMP_object->set_int_position(joint_default_state[active_dofs[i]].th, i-1);  // set DMP to init robot position
    }
    
    // go to the target
    bool there = true;
    for (int i = 1; i <= B_HR; i++)
        if (fabs(target_[i].th - joint_des_state[i].th) > 1.0e-2)
        {
            there = false;
            break;
        }
    if (!there)
        if (!go_target_wait_ID(target_))
        {
            return FALSE;
        }
    printf("Initial position reached.\n");

    // ready to go
    ans = 999;
    while (ans == 999) {
        if (!get_int(const_cast<char*>("Enter 1 to start or anthing else to abort ..."),ans,&ans))
        {
            return FALSE;
        }
    }
    
    // only go when user really types the right thing
    if (ans != 1)
    {
        return FALSE;
    }
    
    start_time_ = task_servo_time;
    printf("start time = %.3f, task_servo_time = %.3f\n", start_time_, task_servo_time);
    
    //

    return TRUE;
}

int coaching_pDMP_Task::run()
{
    int i, j, k, rank;
    double task_time = task_servo_time - start_time_;
    double dt = 1.0 / (double) servo_rate;
    
    double tmp_tau, tmp_phi;
    
    static double y[3] = {0, 0, 0};
    double yd[3];
    
    double Ct_x[3], Ct_q[7];
    
    float ball[9];
    
    double o_R_[3], o_L_[3], dir_tmp[3], dir_tmp2[3], o_tmp[3];
    double dir_norm, dist_R, dist_L;
    
    
    if (KinectReader(NULL, buffer) == 720)
    {
      kinect2direction(buffer);
    }
    
    // Get cartesian position and velocty
    for (int i = 0; i < 3; i++)
    {
        yd[i] = y[i]; // store previus value
        y[i] = cart_state[RIGHT_HAND].x[i+1];  //_X_ = 1 ...
        yd[i] = (y[i] - yd[i]) / dt;
    }
    
    
    if( real_robot_flag )
    {
        
        for (int i = 0; i < 3; i++)
        {
            o_R_[i] = joint_pose[8][i][3];
            o_L_[i] = joint_pose[5][i][3];
            o_tmp[i] = o[i];
        }
        KinectToCbi(o_R_, o_R);
        KinectToCbi(o_L_, o_L);
        
        dist_R = sqrt(SQR(o_R[0] - y[0]) + SQR(o_R[1] - y[1]) + SQR(o_R[2] - y[2]));
        dist_L = sqrt(SQR(o_L[0] - y[0]) + SQR(o_L[1] - y[1]) + SQR(o_L[2] - y[2]));
        
        if (dist_R < dist_L)
        {
            for (int i = 0; i < 3; i++)
                dir_tmp[i] = joint_pose[7][i][3];
            
            KinectToCbi(dir_tmp, dir_tmp2);
            
            dir_norm = sqrt(SQR(o_R[0] - dir_tmp2[0]) + SQR(o_R[1] - dir_tmp2[1]) + SQR(o_R[2] - dir_tmp2[2]));
            
            if (dir_norm > epsilon)
            {
                for (int i = 0; i < 3; i++)
                    dir[i] = (o_R[i] - dir_tmp2[i]) / dir_norm;
                for (int i = 0; i < 3; i++)
                    o[i] = o_R[i] + (distance * dir[i]);
            }
            else
            {
                for (int i = 0; i < 3; i++)
                    o[i] = o_tmp[i];
            }
        }
        else
        {
            for (int i = 0; i < 3; i++)
                dir_tmp[i] = joint_pose[4][i][3];
            
            KinectToCbi(dir_tmp, dir_tmp2);
            
            dir_norm = sqrt(SQR(o_L[0] - dir_tmp2[0]) + SQR(o_L[1] - dir_tmp2[1]) + SQR(o_L[2] - dir_tmp2[2]));
            
            if (dir_norm > epsilon)
            {
                for (int i = 0; i < 3; i++)
                    dir[i] = -1.0 * (o_L[i] - dir_tmp2[i]) / dir_norm;
                for (int i = 0; i < 3; i++)
                    o[i] = o_L[i] - ( 2.0 * distance * dir[i] );
            }
            else
            {
                for (int i = 0; i < 3; i++)
                    o[i] = o_tmp[i];
            }
        }
    }
    else
    {
        dist_R = sqrt(SQR(o_R[0] - y[0]) + SQR(o_R[1] - y[1]) + SQR(o_R[2] - y[2]));
        dist_L = sqrt(SQR(o_L[0] - y[0]) + SQR(o_L[1] - y[1]) + SQR(o_L[2] - y[2]));
        
        if (dist_R < dist_L)
        {
            for (int i = 0; i < 3; i++)
                dir[i] = dir_set[i];
            
            for (int i = 0; i < 3; i++)
                o[i] = o_R[i] + distance * dir[i];
        }
        else
        {
            for (int i = 0; i < 3; i++)
                o[i] = o_L[i] + ( 2.0 * distance * dir_set[i]);
            for (int i = 0; i < 3; i++)
                dir[i] = -1.0 * dir_set[i];
        }
    }
    
    /* if (!(step % 500))
    {
        // printf("%e %e %e\n", cart_state[RIGHT_HAND].x[1], cart_state[RIGHT_HAND].x[2], cart_state[RIGHT_HAND].x[3]);
        printf("%.2f %.2f %.2f\n", o[0], o[1], o[2]);
        printf("%.2f %.2f %.2f\n", o_R[0], o_R[1], o_R[2]);
        printf("%.2f %.2f %.2f\n\n", o_L[0], o_L[1], o_L[2]);
    }*/
    
    // Transform result into world coordinates
    for (i = 0; i < 3; i++)
    {
        ball[i] = o[i];
        ball[3+i] = o_R[i];
        ball[6+i] = o_L[i];
    }
    
    sendUserGraphics((char *) "ball2", ball, sizeof(ball));
    
    // Calculate AFS stuff
    AFS_object->AFS_integrate(dt);
    
    // Couple AFS with pDMP
    tmp_tau = 1 / (AFS_object->get_frequency(0));
    tmp_phi = AFS_object->get_phase(0);
    
    for (int i = 0; i < active_dofs[0]; i++){
        pDMP_object->set_phase(i, tmp_phi);
        pDMP_object->set_tau(i,tmp_tau);
    }
    // Calculate pDMP stuff
    if (pDMP_object->get_flag() == 2)
    {
        pDMP_object->Ct(y, yd, o, dir, Ct_x);  // get Ct in cartesian space
 
        pDMP_object->Ct_to_Joint(active_dofs, Ct_x, Ct_q); // Recalculate Ct into joint space

        pDMP_object->pDMP_calculate_f(Ct_q); // Update the pDMPs
    }
    else
    {
        pDMP_object->pDMP_calculate_f();
    }
    
    pDMP_object->pDMP_integrate(dt);
    
    for (int i = 1; i <= active_dofs[0]; ++i) {
        target_[active_dofs[i]].th = pDMP_object->get_position(i-1);
        target_[active_dofs[i]].thd = pDMP_object->get_velocity(i-1);
        target_[active_dofs[i]].thdd = 0;
    }
    
    // the following variables need to be assigned
    for (int i = 1; i <= N_DOFS; ++i) {
        joint_des_state[i].th = target_[i].th;
        joint_des_state[i].thd = target_[i].thd;
        joint_des_state[i].thdd = target_[i].thdd;
        joint_des_state[i].uff = 0.0;
    }

    // Print something with 1Hz
    //if (!(step % servo_rate))
    if ((step % servo_rate) == servo_rate)
    {
        //for (int i=0; i<3; i++)
        //    printf("%f, ", dir[i]);
        //printf("\n");
        printf("Des: %.2f, DMP: %.2f \n" ,joint_des_state[R_SFE].th,  pDMP_object->get_position(0));
        //temp_freq = pDMP_object->vector_angle(tmp, tmpb);
        //printf("Avoiding cart vel: %f, %f, %f \n", tmpb[0], tmpb[1], tmpb[2]);
        //printf("Norm: %f, %f, %f \n", yd[0], yd[1], yd[2]);
        //printf("Norm: %f \n", temp_freq);
    }
    
        
    step++;

    return TRUE;
}

int coaching_pDMP_Task::changeParameters()
{
    static int ans = 0;
    double temp_freq;
    int temp_flag;
    

    int numPairs = 3;
    
    do
    {
        ans = 0;
        printf("\nChoose what to change (values in parentheses are current values):\n\n");
        printf("                                                          Do nothing --> 0\n");
        printf("   -----------------------------------------------------------------------\n");
        printf("                                      Set new frequency  (%.2f) [Hz] --> 1\n", AFS_object->get_frequency(0)/PI);
        printf("   -----------------------------------------------------------------------\n");
        printf("                          Print current states for the peridoic DMP  --> 2\n");
        printf("                                                  Save peridoic DMP  --> 3\n");
        printf("                                                  Load peridoic DMP  --> 4\n");
        printf("   -----------------------------------------------------------------------\n");
        printf("                       2 - Activate / 0 - Deactivate coaching || (%d) --> 5\n", pDMP_object->get_flag());
        printf("                       Change perturbation direction (%.2f, %.2f, %.2f) --> 6\n", dir_set[0], dir_set[1], dir_set[2]);
        printf("                        Print perturbation direction (%.2f, %.2f, %.2f) --> 7\n", dir[0], dir[1], dir[2]);
        printf("              Input perturbation position RIGHT HAND (%.2f, %.2f, %.2f) --> 8 \n", o_R[0], o_R[1], o_R[2]);
        printf("               Input perturbation position LEFT HAND (%.2f, %.2f, %.2f) --> 9 \n", o_L[0], o_L[1], o_L[2]);
        printf("                                        Perturbation offset (%.2f)  --> 10\n", distance);
        printf("   -----------------------------------------------------------------------\n");
        printf("                           Print calibration matrix and translation --> 11\n");
        if (!get_int(const_cast<char*>("\n   -------> Input "), ans, &ans))
        {
            return FALSE;
        }
        if (ans < 0 || ans > 10)
            printf("\n\nERROR: Invalid selection\n\n");
    } while (ans < 0 || ans > 10);
    switch (ans)
    {
        case 1:
            get_double(const_cast<char*>("Set frequency:"),temp_freq,&temp_freq);
            AFS_object->set_frequency(0,temp_freq*PI);
            break;
        case 2:
            pDMP_object->pDMP_param_print();
            break;
        case 3:
            pDMP_object->save_DMP(DMP_file_name_new);
            break;
        case 4:
            pDMP_object->load_DMP(DMP_file_name);
            break;
        case 5:
            temp_flag = pDMP_object->get_flag();
            get_int(const_cast<char*>("Set flag:"),temp_flag,&temp_flag);
            if (temp_flag < 0 || temp_flag > 2)
                printf("\n\nERROR: Invalid selection\n\n");
            else
            {
                pDMP_object->set_flag(temp_flag);
            }

            break;
        case 6:
            get_double(const_cast<char*>("X: "),dir_set[0],&dir_set[0]);
            get_double(const_cast<char*>("Y: "),dir_set[1],&dir_set[1]);
            get_double(const_cast<char*>("Z: "),dir_set[2],&dir_set[2]);
            break;
        case 7:
            printf("Perturbation direction (%.2f, %.2f, %.2f)\n", dir[0], dir[1], dir[2]);
            break;
        case 8:
            get_double(const_cast<char*>("X: "),o_R[0],&o_R[0]);
            get_double(const_cast<char*>("Y: "),o_R[1],&o_R[1]);
            get_double(const_cast<char*>("Z: "),o_R[2],&o_R[2]);
            break;
        case 9:
            get_double(const_cast<char*>("X: "),o_L[0],&o_L[0]);
            get_double(const_cast<char*>("Y: "),o_L[1],&o_L[1]);
            get_double(const_cast<char*>("Z: "),o_L[2],&o_L[2]);
            break;
        case 10:
            get_double(const_cast<char*>("Perturbation offset: "),distance,&distance);
            break;
        case 11:
            print_Kinect();
            
            break;
        default:
            break;
    }
    return TRUE;
}

void coaching_pDMP_Task::kinect2direction(char buffer[])
{
int i, j, k;
char *buffer_pt;
    
    buffer_pt = &(buffer[0]);
    for (i = 0; i < 15; i++)
    {
        for (j = 0; j < 3; j++)
        {
            joint_pose[i][j][3] = ((float *) buffer_pt)[0] / 1000.0;
            buffer_pt += 4;
        }
        for (k = 0; k < 3; k++)
            for (j = 0; j < 3; j++)
            {
                joint_pose[i][j][k] = ((float *) buffer_pt)[0];
                buffer_pt += 4;
            }
    }

    
    if ((count_Kinect_frames % 10) == 0)
    {
        /*for (k = 0; k < 3; k++)
            printf("%lf ", joint_pose[7][k][3]);
        printf("\n");
        
        for (k = 0; k < 3; k++)
            printf("%lf ", joint_pose[8][k][3]);
        printf("\n\n");
        */
         
        /*
        printf("Counter %d\n", count_Kinect_frames);
        for (i = 7; i < 8; i++)
        {
            for (j = 0; j < 3; j++)
            {
                for (k = 3; k < 4; k++)
                    printf("%lf ", joint_pose[i][j][k]);
                printf("\n");
            }
            printf("\n");
        }
        printf("\n\n");
         */
    }
    
    
    
    
    ///// RIGHT ARM //////////////////////////////////////////////////////////
    
    // Shoulder:
    // Elbow:
    /* elbNum = (joint_pose[7][0][3]-joint_pose[6][0][3]) * (joint_pose[7][0][3]-joint_pose[8][0][3]) +
    (joint_pose[7][1][3]-joint_pose[6][1][3]) * (joint_pose[7][1][3]-joint_pose[8][1][3]) +
    (joint_pose[7][2][3]-joint_pose[6][2][3]) * (joint_pose[7][2][3]-joint_pose[8][2][3]);
    elbDen = sqrt(my_SQR(joint_pose[7][0][3]-joint_pose[6][0][3]) +
                  my_SQR(joint_pose[7][1][3]-joint_pose[6][1][3]) +
                  my_SQR(joint_pose[7][2][3]-joint_pose[6][2][3])) *
    sqrt(my_SQR(joint_pose[7][0][3]-joint_pose[8][0][3]) +
         my_SQR(joint_pose[7][1][3]-joint_pose[8][1][3]) +
         my_SQR(joint_pose[7][2][3]-joint_pose[8][2][3])); */
    
    ///// LEFT ARM /////////////////////////////////////////////////////

    // Elbow:
    /* elbNum = (joint_pose[4][0][3]-joint_pose[3][0][3]) * (joint_pose[4][0][3]-joint_pose[5][0][3]) +
    (joint_pose[4][1][3]-joint_pose[3][1][3]) * (joint_pose[4][1][3]-joint_pose[5][1][3]) +
    (joint_pose[4][2][3]-joint_pose[3][2][3]) * (joint_pose[4][2][3]-joint_pose[5][2][3]);
    elbDen = sqrt(my_SQR(joint_pose[4][0][3]-joint_pose[3][0][3]) +
                  my_SQR(joint_pose[4][1][3]-joint_pose[3][1][3]) +
                  my_SQR(joint_pose[4][2][3]-joint_pose[3][2][3])) *
    sqrt(my_SQR(joint_pose[4][0][3]-joint_pose[5][0][3]) +
         my_SQR(joint_pose[4][1][3]-joint_pose[5][1][3]) +
         my_SQR(joint_pose[4][2][3]-joint_pose[5][2][3])); */
}

int coaching_pDMP_Task::KinectReader(const char *fifo_name, char *buf)
{
    int i, j, n, n_tmp;
    
    // Open pipe to receive data from Kinect
    if (fifo_name != NULL)
    {
        printf("Waiting to open Kinect pipe %s!\n", fifo_name);
        Kinect_pipe = open(fifo_name, O_RDONLY | O_NONBLOCK);
        if (Kinect_pipe <= 0)
            
        {
            printf("Kinect pipe could not be opened!\n");
            Kinect_pipe = 0;
            return 0;
        }
        printf("Kinect pipe opened for reading, pipe ID %d!\n", Kinect_pipe);
        
        return 1;
    }
    
    // Receive data
    if (Kinect_pipe == 0)
        return 0;
    else
    {
        n = 0;
        do
        {
            n_tmp = read(Kinect_pipe, buf, MAX_BUFFER);
            if (n_tmp == 720)
            {
                n = n_tmp;
                if (n < MAX_BUFFER)
                    buf[n] = '\0';
                count_Kinect_frames++;
            }
        }
        while (n_tmp > 0);
        
        /* if ((task_servo_steps % 500) == 500)
        {
            //printf("time: %lf, number of packets: %d, %d\n", task_servo_time, packet_count, n);
        } */
    }
    
    return n;
}


void coaching_pDMP_Task::matMultAB(double A[3][3], double B[3][3], double C[3][3])
{
    int i, j, k;
    for (i = 0; i < 3; i++)
        for (j = 0; j < 3; j++)
        {
            C[i][j] = 0;
            for (k = 0; k < 3; k++)
                C[i][j] += A[i][k]*B[k][j];
        }
}

void coaching_pDMP_Task::matMultABt(double A[3][3], double B[3][3], double C[3][3])
{
    int i, j, k;
    for (i = 0; i < 3; i++)
        for (j = 0; j < 3; j++)
        {
            C[i][j] = 0;
            for (k = 0; k < 3; k++)
                C[i][j] += A[i][k]*B[j][k];
        }
}

void coaching_pDMP_Task::matMultABt_NUMPAIRS(double A[3][NUMPAIRS], double B[3][NUMPAIRS], double C[3][3])
{
    int i, j, k;
    for (i = 0; i < 3; i++)
        for (j = 0; j < 3; j++)
        {
            C[i][j] = 0;
            for (k = 0; k < NUMPAIRS; k++)
                C[i][j] += A[i][k]*B[j][k];
        }
}



double coaching_pDMP_Task::CalcDet(double a[3][3])
{
    /*
    return a[0][0]*a[1][1]*a[2][2] + a[0][1]*a[1][2]*a[2][0] + a[0][2]*a[1][0]*a[1][2]
    - a[0][2]*a[2][2]*a[2][0] - a[0][1]*a[1][0]*a[2][2] - a[0][0]*a[1][2]*a[2][1];*/
    
    return a[0][0]*(a[1][1]*a[2][2] - a[1][2]*a[2][1]) - a[0][1] *(a[1][0]*a[2][2] -a[1][2]*a[2][0] ) + a[0][2] *(a[1][0]*a[2][1] - a[1][1]*a[2][0]);
}

/*============================================================================
 ==============================================================================
 
 Calulate rotation matrix R and translation vector t from calibration data
 Inputs:
 p - matrix 3xn, of data points in basic frame
 q - matrix 3xn, of coresponding data points in differnet frame
 numPairs - number of data pairs (n)
 
 Output:
 R - Rotational matrix
 t - Translational vector
 
 ==============================================================================
 Remarks: All data pairs have the same weights
 ============================================================================*/

void coaching_pDMP_Task::KinectTransformationMatrix(double p[NUMPAIRS][3], double q[NUMPAIRS][3],double R[3][3], double t[3]){
    double p_tilda[3];
    double q_tilda[3];
    
    
    
    double S[3][3]; //[numPairs][numPairs];
    
    double x[3][NUMPAIRS], y[3][NUMPAIRS]; //[3][numPairs], y[3][numPairs];
    //double X, Y;
    
    double V_[3][3];
    double U_[3][3];
    double R_temp[3][3], R_temp2[3][3];
    double t_temp[3];
    double VU_determinat;
    
    Matrix A, V;
    Vector s;
    
    int i,j,k;
    
    for(i = 0; i<3; i++)
    {
        p_tilda[i] = 0;
        q_tilda[i] = 0;
        for(j = 0; j<NUMPAIRS; j++)
        {
            p_tilda[i] += p[j][i];
            q_tilda[i] += q[j][i];
        }
        p_tilda[i] = p_tilda[i]/NUMPAIRS;
        q_tilda[i] = q_tilda[i]/NUMPAIRS;
    }
    
    for(i = 0; i<3; i++)
    {
        for(j = 0; j<NUMPAIRS; j++)
        {
            x[i][j] = p[j][i] - p_tilda[i];
            y[i][j] = q[j][i] - q_tilda[i];
        }
    }
    
     
    matMultABt_NUMPAIRS(x, y, S);

    
     
    //Prepare matrices for SVD
    A = my_matrix(1, 3, 1, 3);
    V = my_matrix(1, 3, 1, 3);
    s = my_vector(1, 3);
    
    for(i=0; i<3; i++)
        for(j=0; j<3; j++)
            A[i+1][j+1] = S[i][j];
    
    // Note that A is repplayest by U and V and not transpose V' is returned!!!
    my_svdcmp(A, 3, 3, s, V);
    
    // Write them back
    for(i=0; i<3; i++)
    {
        for(j=0; j<3; j++)
        {
            V_[i][j] = V[i+1][j+1];
            U_[i][j] = A[i+1][j+1];
        }
    }

    matMultABt(V_, U_, R_temp);
    
    VU_determinat = CalcDet(R_temp);
    //R_temp[0][0]*R_temp[1][1]*R_temp[2][2] + R_temp[0][1]*R_temp[1][2]*R_temp[2][0] + R_temp[0][2]*R_temp[1][0]*R_temp[1][2]
    //- R_temp[0][2]*R_temp[2][2]*R_temp[2][0] - R_temp[0][1]*R_temp[1][0]*R_temp[2][2] - R_temp[0][0]*R_temp[1][2]*R_temp[2][1];
    

    for(i=0; i<3; i++)
    {
        for(j=0; j<3; j++)
        {
            if(i==j)
                R_temp[i][j] = 1.0;
            else
                R_temp[i][j] = 0.0;
        }
    }
    R_temp[2][2] = VU_determinat;
    
    matMultAB(V_, R_temp, R_temp2);
    
    matMultABt(R_temp2, U_, R);
    
    
    for(i=0; i<3; i++)
    {
        t_temp[i] = 0;
        for(j=0; j<3; j++)
        {
            t_temp[i] += R[i][j]*p_tilda[j];
        }
        t[i] = q_tilda[i] - t_temp[i];
    }
    
}

void coaching_pDMP_Task::KinectToCbi(double p[3], double p_out[3])
{
int i, k;
    for (i = 0; i < 3; i++)
    {
        p_out[i] = tran[i];
        for (k = 0; k < 3; k++)
            p_out[i] += Rota[i][k]*p[k];
    }
}


int coaching_pDMP_Task::Calibrate_Procedure(){
    
    int ans = 999;

    // prepare going to the default posture
    bzero((char *)&(target_[1]),N_DOFS*sizeof(target_[1]));
    for (int i=1; i<=N_DOFS; i++)
    {
        target_[i] = joint_default_state[i];
    }
    
    target_[R_SFE].th = 80.0 / 180.0 * PI;
    target_[R_SAA].th = -30.0 / 180.0 * PI;
    target_[R_EB].th = 20.0 / 180.0 * PI;
  
    // go to the target
    bool there = true;
    for (int i = 1; i <= B_HR; i++)
        if (fabs(target_[i].th - joint_des_state[i].th) > 1.0e-2)
        {
            there = false;
            break;
        }
    
    if (!there)
        if (!go_target_wait_ID(target_))
        {
            return FALSE;
        }
    printf("Position 1 for calibration reached.\n");
    
    
    // ready to go
    while (ans != 1) {
        if (!get_int(const_cast<char*>("Enter 1 to get Kinect position ..."),ans,&ans))
        {
            return FALSE;
        }
    }
    if( real_robot_flag ) {
        while (ans != 3)
        {
            if (KinectReader(NULL, buffer) == 720)
            {
                kinect2direction(buffer);
                ans++;
            }
        }
    }
    
    // Reset ans
    ans = 999;
    
    printf("\n");
    for (int i = 0; i < 3; i++)
    {
        p[0][i] = joint_pose[8][i][3];
        q[0][i] = cart_state[RIGHT_HAND].x[i+1];
        
        printf("%.2f, (%.2f)", p[0][i],q[0][i]);
    }
    printf("\n");
    
    
    // Second position for calibration...
    
    // prepare going to the default posture
    bzero((char *)&(target_[1]),N_DOFS*sizeof(target_[1]));
    for (int i = 1; i <= N_DOFS; i++)
    {
        target_[i] = joint_default_state[i];
    }
    
    target_[R_SFE].th = 20.0 / 180.0 * PI;
    target_[R_SAA].th = -80.0 / 180.0 * PI;
    target_[R_EB].th = 20.0 / 180.0 * PI;
    
    
    // go to the target
    there = true;
    for (int i = 1; i <= B_HR; i++)
        if (fabs(target_[i].th - joint_des_state[i].th) > 1.0e-2)
        {
            there = false;
            break;
        }
    
    if (!there)
        if (!go_target_wait_ID(target_))
        {
            return FALSE;
        }
    printf("Position 2 for calibration reached.\n");
    
    
    // ready to go
    while (ans != 1) {
        if (!get_int(const_cast<char*>("Enter 1 to get Kinect position ..."),ans,&ans))
        {
            return FALSE;
        }
    }
    
    if( real_robot_flag )
    {
        while(ans != 3){
            if (KinectReader(NULL, buffer) == 720)
            {
                kinect2direction(buffer);
                ans++;
            }
        }
    }
        
    // Reset ans
    ans = 999;
    
    
    printf("\n");
    for(int i = 0; i < 3; i++)
    {
        p[1][i] = joint_pose[8][i][3];
        q[1][i] = cart_state[RIGHT_HAND].x[i+1];
        
        printf("%.2f, (%.2f)  ", p[1][i],q[1][i]);
    }
    printf("\n");
    
    
    
    // Third position for calibration...
        
    // prepare going to the default posture
    bzero((char *)&(target_[1]),N_DOFS*sizeof(target_[1]));
    for (int i = 1; i <= N_DOFS; i++)
    {
        target_[i] = joint_default_state[i];
    }
    
    target_[R_HR].th += 30.0 / 180.0 * PI;
    
    
    // go to the target
    there = true;
    for (int i = 1; i <= B_HR; i++)
        if (fabs(target_[i].th - joint_des_state[i].th) > 1.0e-2)
        {
            there = false;
            break;
        }
    
    if (!there)
        if (!go_target_wait_ID(target_))
        {
            return FALSE;
        }
    printf("Position 3 for calibration reached.\n");
    
    
    // ready to go
    while (ans != 1) {
        if (!get_int(const_cast<char*>("Enter 1 to get Kinect position ..."),ans,&ans))
        {
            return FALSE;
        }
    }
    
    if( real_robot_flag )
    {
        while(ans != 3){
            if (KinectReader(NULL, buffer) == 720)
            {
                kinect2direction(buffer);
                ans++;
            }
        }
    }
    // Reset ans
    ans = 999;
    
    
    printf("\n");
    for(int i = 0; i < 3; i++)
    {
        p[2][i] = joint_pose[8][i][3];
        q[2][i] = cart_state[RIGHT_HAND].x[i+1];
        
        printf("%.2f, (%.2f)  ", p[2][i],q[2][i]);
    }
    printf("\n");
    
    
    // prepare going to the default posture
    bzero((char *)&(target_[1]),N_DOFS*sizeof(target_[1]));
    for (int i = 1; i <= N_DOFS; i++)
    {
        target_[i] = joint_default_state[i];
    }
    
    // go to the target
    there = true;
    for (int i = 1; i <= B_HR; i++)
        if (fabs(target_[i].th - joint_des_state[i].th) > 1.0e-2)
        {
            there = false;
            break;
        }
    
    if (!there)
        if (!go_target_wait_ID(target_))
        {
            return FALSE;
        }
    printf("Position 4 for calibration reached.\n");
    
    
    // ready to go
    while (ans != 1) {
        if (!get_int(const_cast<char*>("Enter 1 to get Kinect position ..."),ans,&ans))
        {
            return FALSE;
        }
        
    }
    
    if( real_robot_flag )
    {
        while(ans != 3){
            if (KinectReader(NULL, buffer) == 720)
            {
                kinect2direction(buffer);
                ans++;
            }
        }
    }
    // Reset ans
    ans = 999;
    
    printf("\n");
    for(int i=0; i<3; i++)
    {
        p[3][i] = joint_pose[8][i][3];
        q[3][i] = cart_state[RIGHT_HAND].x[i+1];
        
        printf("%.2f, (%.2f)", p[3][i],q[3][i]);
    }
    printf("\n");
    
    // Calculate transformation matrix !!!
    
    KinectTransformationMatrix(p, q, Rota, tran);
    
    for(int i=0 ; i<3 ; i++){
        for(int j=0 ; j<3 ; j++){
            printf("%f, ", Rota[i][j]);
        }
        printf("| %f \n", tran[i]);
    }
    return TRUE;
}



void coaching_pDMP_Task::save_Kinect(char *file_name)
{
    FILE *f_ = NULL;
    int i, j;
    
    if (file_name != NULL)
        f_ = fopen(file_name, "w");
    
    if (f_ != NULL)
    {
        for (j = 0; j < 3; j++)
        {
            for (i=0; i < 3; i++)
                fprintf(f_, "%le ", Rota[j][i]);
        }
        fprintf(f_, "\n");

        for (i=0; i < 3; i++)
            fprintf(f_, "%le ", tran[i]);
        fprintf(f_, "\n");
        
        fclose(f_);
    }
}


void coaching_pDMP_Task::load_Kinect(char *file_name)
{
    FILE *f_ = NULL;
    int i, j, k;
    char line[STRING_MAX];
    
    if (file_name != NULL)
        f_ = fopen(file_name, "r");
    
    if (f_ != NULL)
    {
        
        k = 0;
        for (j = 0; j < 3; j++)
        {
            for (i = 0; i < 3; i++)
                k += fscanf(f_, "%le", &(Rota[j][i]));
        }
        if (k != 9)
        {
            printf("File ended too quickly!!");
            goto missing_data;
        }
        
        k = 0;
        for (j = 0; j < 3; j++)
        {
            k += fscanf(f_, "%le", &(tran[j]));
        }
        if (k != 3)
        {
            printf("File ended too quickly");
            goto missing_data;
        }
        fclose(f_);
        return;
    }
    missing_data:
    fclose(f_);
}


void coaching_pDMP_Task::print_Kinect()
{
    int i,j;
    double Rtemp[3][3];
    double dettemp;
    
    for(int i=0 ; i<3 ; i++){
        for(int j=0 ; j<3 ; j++){
            printf("%.2f, ", Rota[i][j]);
        }
        printf("| %.2f \n", tran[i]);
    }
    
    matMultABt(Rota, Rota, Rtemp);
    dettemp = CalcDet(Rota);
    
    for(int i=0 ; i<3 ; i++){
        for(int j=0 ; j<3 ; j++){
            printf("%.2f, ", Rtemp[i][j]);
        }
    }
    
    printf("Det: %.2f \n", dettemp);
    
    
}
















