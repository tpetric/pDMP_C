/*!*****************************************************************************
 *******************************************************************************

 \note  pDMP_structure.cpp
 \date  August 2013
 \authors: Tadej Petric

 \remarks

 Periodic dynamic movement primitives and incremental locally weighted
 regression.
 
 REFERENCE: 2012 Intech Gams, Petric and others

 ******************************************************************************/

#include <math.h>
#include <stddef.h>
#include <sys/time.h>
#include <string.h>

#include "pDMPstructure.h"

#define pi 3.14159265358979323846

#define SQR(a) ((a)*(a))

using namespace std;


pDMP_structure::pDMP_structure(int dof_, int N_, double alpha_z_, double beta_z_, double lambda_)
{
    int i, j, k;

    flag = 0;
    dof = dof_; N = N_;
    //printf("dof = %d, N = %d, flag = %d\n", dof, N, flag);
  
    alpha_z = alpha_z_; beta_z = beta_z_; lambda = lambda_;
    //printf("alpha_z = %.2lf, beta_z = %.2lf, lambda = %.2lf\n", alpha_z, beta_z, lambda);

    //Clear variable space
    g = c = f = r = NULL;
    
    phi = tau = NULL;
    
    y = z = NULL;
    
    w = NULL;
    P = NULL;
  
    // Clear coaching related variables
    A = V = NULL;
    s = b = dtheta = NULL;

    
    //Define variables
    g = new double[dof];
    c = new double[N];
    f = new double[dof];
    
    r = new double[dof];
    
    phi = new double[dof];
    tau = new double[dof];
  
    y = new double[dof];
    z = new double[dof];

    // Set default states
    for (i = 0; i < dof ; i++)
    {
        r[i] = 1;
        g[i] = 0;
        y[i] = 0;
        z[i] = 0;
    }
    
    /*
    y_in = new double[dof];
    dy_in = new double[dof];
    ddy_in = new double[dof];
    */
    
    w = new double *[dof];
    P = new double *[dof];
    for (i = 0; i < dof; i++){
        w[i] = new double[N];
        P[i] = new double[N];
        // set inital variables
        for (j = 0; j < N; j++ )
        {
            w[i][j] = 0;
            P[i][j] = 1;
        }
    }
    
    // basis functions are also defined here
    define_basis_functions(N, 0.5);
        
}

pDMP_structure::~pDMP_structure()
{
    int num;
    if (g != NULL)
        delete [] g;
    if (c != NULL)
        delete [] c;
    if (f != NULL)
        delete [] f;
    if (r != NULL)
        delete [] r;
  
    if (tau != NULL)
        delete [] tau;
    if (phi != NULL)
        delete [] phi;

    if (y != NULL)
        delete [] y;
    if (z != NULL)
        delete [] z;
    
    /*
    if (y_in != NULL)
        delete [] y_in;
    if (dy_in != NULL)
        delete [] dy_in;
    if (ddy_in != NULL)
        delete [] ddy_in;
    */
     
    if (w != NULL)
    {
        for (int i = 0; i < dof; i++)
            delete [] w[i];
        delete [] w;
    }
    
    if (P != NULL)
    {
        for (int i = 0; i < dof; i++)
            delete [] P[i];
        delete [] P;
    }
}


void pDMP_structure::pDMP_param_print(void)
{
    int i, j;
    printf("   --- Periodic DMP ------------------------------------------------------\n");
    printf("\ndof = %d, N = %d, flag = %d\n", dof, N, flag);
    printf("alpha_z = %.2lf, beta_z = %.2lf, lambda = %.5lf\n", alpha_z, beta_z, lambda);
    
    if (g != NULL) {
        printf("goal = (");
        for (j = 0; j < dof-1; j++)
        {
            printf("%.2lf, ", g[j]);
        }
        printf("%.2lf)\n\n", g[dof-1]);
    }

    if (g != NULL) {
        printf("r = (");
        for (j = 0; j < dof-1; j++)
        {
            printf("%.2lf, ", r[j]);
        }
        printf("%.2lf)\n\n", r[dof-1]);
    }
    
    if (c != NULL) {
        for (i = 0; i < N; i++)
        {
            printf("%le ", c[i]);
        }
        printf("c\n");
    }

    if (P != NULL) {
        for (i = 0; i < N; i++)
        {
            for (j = 0; j < dof; j++)
            {
                printf("%13.6le ", P[j][i]);
            }
            printf("P\n");
        }
    }
    
    if (w != NULL) {
        for (i = 0; i < N; i++)
        {
            for (j = 0; j < dof; j++)
            {
                printf("%13.6le ", w[j][i]);
            }
            printf("w\n");
        }
    }
}

void pDMP_structure::pDMP_calculate_f(double *y_in, double *dy_in, double *ddy_in)
{
    int i, j, k;
    double target[dof];
    double psi[N];
    double Pu;
    double sum_psi, sum_all;
    
    for(i = 0; i < dof ; i++)
    {
        // calculate target for fitting
        target[i] = tau[i]*tau[i]*ddy_in[i] - alpha_z*(beta_z*(g[i]-y_in[i])-tau[i]*dy_in[i]);
        
        sum_all = 0; sum_psi = 0;
        
        // recursie regression
        for (j = 0; j < N ;j++)
        {
            // Update weights
            psi[j] = exp(2.5*N* (cos(phi[i]-c[j])-1));
            Pu = P[i][j];
            Pu = (Pu - (Pu*Pu*r[i]*r[i]) / (lambda/psi[j] + r[i]*r[i]*Pu))/lambda ;
            w[i][j] += psi[j]*Pu*r[i]*(target[i]-w[i][j]*r[i]);
            P[i][j] = Pu;
            // Recunstruct DMP
            sum_psi += psi[j];
            sum_all += w[i][j]*r[i]*psi[j];
        }
        if ( sum_psi == 0 )
            f[i] = 0;
        else
        {
            f[i] = sum_all/sum_psi;
        }
    }
}


void pDMP_structure::pDMP_calculate_f(void)
{
    int i, j;
    double psi[N];
    double sum_psi, sum_all;
    
    for(i = 0; i < dof ; i++)
    {
        sum_all = 0; sum_psi = 0;
        
        // recursie regression
        for (j = 0; j < N ;j++)
        {
            psi[j] = exp(2.5*N* (cos(phi[i]-c[j])-1));
            // Recunstruct DMP
            sum_psi += psi[j];
            sum_all += w[i][j]*r[i]*psi[j];
        }
        if ( sum_psi == 0 )
            f[i] = 0;
        else
        {
            f[i] = sum_all/sum_psi;
        }
    }
}


void pDMP_structure::pDMP_integrate(double dt)
{
    int i, j, k;
    
    double dz, dy;
    
    const int servo_rate = 1; //SERVO_BASE_RATE / TASK_SERVO_RATIO;
    double desired_dt = 1.0 / servo_rate;
    int steps;
  
    steps = (int) (dt / desired_dt + 0.5);
  
    dt = dt / steps;

    
    // Integrate fourier series coeficients using Euler's method
    for (k = 0; k < steps; k++)
    {
        for (j = 0; j < dof; j++)
        {
            dz = (1/tau[j])*(alpha_z *(beta_z *(g[j]-y[j])-z[j]) + f[j]);
            dy = (1/tau[j])*z[j];
            
            //Euler integration - new frequency and phase
            z[j] += dz * dt;
            y[j] += dy * dt;
        }
    }
}


double pDMP_structure::get_position(int j)
{
    return y[j];
}

double pDMP_structure::get_velocity(int j)
{
    return (z[j] / tau[j]);
}

double pDMP_structure::get_acceleration(int j)
{
    return 0;  // To fool the tall good and smart system manager.
}

void pDMP_structure::get_weights(int j, double *output)
{
    
}

void pDMP_structure::set_weights(int j, double *input)
{
    
}


double pDMP_structure::get_phase(int i)
{
    return phi[i];
}

double pDMP_structure::get_tau(int i)
{
    return tau[i];
}

void pDMP_structure::set_phase(int i, double phi_)
{
    phi[i] = phi_;
}

void pDMP_structure::set_tau(int i, double tau_)
{
    tau[i] = tau_;
}




void pDMP_structure::define_basis_functions(int N, double factor)
{
    int i;
    double step;
    const double c_min = 0.0;
    const double c_max = 2*pi;
    
    step = (c_max - c_min)/N;  // similar to linspace
    for (i = 0; i < N ; i++ )
    {
        c[i] = factor * step + step * i;
    }
}


void pDMP_structure::save_DMP(char *file_name)
{
    FILE *f_ = NULL;
    int i, j;
    
    if (file_name != NULL)
        f_ = fopen(file_name, "w");
    
    if (f_ != NULL)
    {
        fprintf(f_, "%d %d %d\n", dof, N, flag);
        
        fprintf(f_, "%le %le %le\n", alpha_z, beta_z, lambda);
        
        fprintf(f_, "\n");  // Do not know why is not working properly without this...
        
        for (j = 0; j < dof; j++)
            fprintf(f_, "%le ", g[j]);
        fprintf(f_, "\n");
        
        for (j = 0; j < dof; j++)
            fprintf(f_, "%le ",r[j]);
        fprintf(f_, "\n");
        
        for (i = 0; i < N; i++)
            fprintf(f_, "%le ",c[i]);
        fprintf(f_, "\n");
        
        for (j = 0; j < dof; j++)
        {
            for (i=0; i < N; i++)
                fprintf(f_, "%le ", w[j][i]);
            fprintf(f_, "\n");
        }
        fclose(f_);
    }
}

void pDMP_structure::load_DMP(char *file_name)
{
    FILE *f_ = NULL;
    int i, j, k;
    char line[STRING_MAX];
    
    if (file_name != NULL)
        f_ = fopen(file_name, "r");
    
    if (f_ != NULL)
    {
        if (fgets(line, STRING_MAX, f_) != NULL)
        {
            double dof_, N_, flag_;
            k = sscanf(line, "%le %le %le\n", &dof_, &N_, &flag_);
            if ( (int) (dof_ + 0.5) != dof)
            {
                printf("Size missmatch");
                goto size_missmatch;
            }
            
            dof = (int) (dof_ + 0.5);
            N = (int) (N_ + 0.5);
            flag = (int) (flag_ + 0.5);
            if (k != 3 || dof < 1 || N < 1)
            {
                printf("k = %d, %d %d %d\n", k, dof, N, flag);
                goto read_error;
            }
        }
        else
        {
            printf("Empty file!\n");
            goto read_error;
        }
        
        if (fgets(line, STRING_MAX, f_) == NULL)
        {
            printf("File ended too quickly!\n");
            goto read_error;
        }
        k = sscanf(line, "%le %le %le", &alpha_z, &beta_z, &lambda);
        if (fgets(line, STRING_MAX, f_) == NULL)
        {
            printf("File ended too quickly!\n");
            goto read_error;
        }
        
        k = 0;
        for (j = 0; j < dof; j++)
        {
            k += fscanf(f_, "%le", &(g[j]));
        }
        if (k != dof)
        {
            printf("Missing goal data\n");
            goto missing_data;
        }
        
        k = 0;
        for (j = 0; j < dof; j++)
        {
            k += fscanf(f_, "%le", &(r[j]));
        }
        if (k != dof)
        {
            printf("DMP amplitude r not in the file!\n");
            goto missing_data;
        }

        k = 0;
        for (i = 0; i < N; i++)
        {
            k += fscanf(f_, "%le", &(c[i]));
        }
        if (k != N)
        {
            printf("DMP basis function centers c not in the file!\n");
            goto missing_data;
        }
        
        k = 0;
        for (j = 0; j < dof; j++)
        {
            for (i = 0; i < N; i++)
                k += fscanf(f_, "%le", &(w[j][i]));
        }
        if (k != dof*N)
        {
            printf("DMP basis function weights w not in the file: %d %d\n", k, dof*N);
            goto missing_data;
        }
        fclose(f_);
        
        for (i = 0; i < dof; i++){
            for (j = 0; j < N; j++ )
            {
                P[i][j] = 1;
            }
        }
        
        return;
    }
    
    read_error:
    
    printf("File %s not read correctly or data wrong!\n", file_name);
    
    if (f != NULL)
        fclose(f_);
    
    return;
    
    missing_data:
    
    printf("Some parameters (c, w, ... and maybe g) initialized to default values!\n");
    
    fclose(f_);
    
    // Set default values
    for (i = 0; i < dof ; i++)
    {
        r[i] = 1;
        g[i] = 0;
        for (j = 0; j < N; j++ )
        {
            w[i][j] = 0;
        }
    }
    
    define_basis_functions(N, 0.5);
    return;
    
    size_missmatch:
    fclose(f_);
}

// ****************************************************************************
// Coatching stuff
// ****************************************************************************

double pDMP_structure::norm(double a[3])
{
    double tmp_a;
    tmp_a = SQR(a[0])+SQR(a[1])+SQR(a[2]);
    return sqrt(tmp_a);
}

double pDMP_structure::sig(double x)
{
    double tmp_a;
    //Potential field parameters
    double ni = 30;
    double d = 0.15;
    //Caluclation
    tmp_a = 1 + exp(ni*(x-d));
    return 1/tmp_a;
}

double pDMP_structure::sig(double x, double d_min, double d_max,double d_off, double ni)
{
    double tmp_min, tmp_max, tmp_off;
    //Potential field parameters
    //double ni = 30;
    ///double d = 0.15;
    //Caluclation
    tmp_min = 1.0 / (1 + exp(ni*(x-d_min)));
    tmp_max = 1.0 / (1 + exp(ni*(x-d_max)));
    tmp_off = 1.0 / (1 + exp(ni*(x-d_off)));
    
    return  tmp_min + tmp_max - tmp_off;
}



void pDMP_structure::vector_norm(double a[3], double a_n[3])
{
    double tmp_a;
    tmp_a = norm(a);
    for(int i=0; i<3; i++)
        a_n[i] = a[i]/tmp_a;
}

double pDMP_structure::vector_angle(double a[3], double b[3])
{
    double tmp_a, tmp_b;
    tmp_a = a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
    tmp_b = norm(a) * norm(b);
    //printf("tmp_a: %f", tmp_a);
    //printf("tmp_b: %f", tmp_b);
    return acos(tmp_a/tmp_b);
}

void pDMP_structure::Ct(double y[3], double dy[3], double o[3], double dir[3], double Ct_[3])
{
    double dist[3];
    double dist_;
    double sig_;
    double exp_;
    double dir_[3];
    double phi;
    // Parametrs
    double beta = 10/PI;
    double gamma = 10;
    
    
    for(int i=0; i<3; i++)
    {
        dist[i] = o[i] - y[i];
        //printf("dist: %f ", dist[i]);
    }
    //printf("%f \n", norm(dist));
        
    vector_norm(dir, dir_);
    
    phi = vector_angle(dist, dy);
    //printf("Phi: %f ", phi*180/PI);
    
    exp_ = exp( -1.0 * beta * phi);
    //printf("exp_: %f ", exp_);
    
    dist_ = norm(dist);
    //printf("dist_: %f ", dist_);
    
    sig_ = sig(dist_);
    //sig_ = sig(dist_, 0.1, 0.35, 0.37, 50);
    //printf("sig_: %f ", sig_);
    
    for(int i=0; i<3; i++)
    {
        Ct_[i] = gamma * sig_ * exp_ * dir_[i];
        //printf("%.3f, ", dy[i]);
    }
    //printf("\n");
}


void pDMP_structure::pDMP_calculate_f(double *Ct_)
{
    int i, j, k;
    double target[dof];
    double psi[N];
    double Pu;
    double sum_psi, sum_all;
    
    for(i = 0; i < dof ; i++)
    {
        sum_all = 0; sum_psi = 0;
        // recursie regression
        for (j = 0; j < N ;j++)
        {
            // Update weights
            psi[j] = exp(2.5*N* (cos(phi[i]-c[j])-1));
            Pu = P[i][j];
            Pu = (Pu - (Pu*Pu*r[i]*r[i]) / (lambda/psi[j] + r[i]*r[i]*Pu))/lambda ;
            w[i][j] += psi[j]*Pu*r[i]*(Ct_[i]);
            P[i][j] = Pu;
            // Recunstruct DMP
            sum_psi += psi[j];
            sum_all += w[i][j]*r[i]*psi[j];
        }
        if ( sum_psi == 0 )
            f[i] = 0;
        else
        {
            f[i] = sum_all/sum_psi;
        }
    }
}

void pDMP_structure::set_int_position(double y_init, int dof){
        y[dof] = y_init;
}



